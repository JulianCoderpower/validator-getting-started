module.exports = function normalizeEmail(validator){
    var email = "TeSt@Te.Com";

    var normalizedEmail = validator.normalizeEmail(email);
    console.log('normalizedEmail: ', normalizedEmail);

    // This method have a second argument
    // to define how the normalize must work
    // { lowercase: true, remove_dots: true, remove_extension: true }
    var emailWithExtension = "TeSt+test-extension@Te.Com";
    var normalizeEmailOptions = {
        lowercase: false,
        remove_dots: false,
        remove_extension: false
    };

    var normalizedEmailWithOptions = validator.normalizeEmail(emailWithExtension, normalizeEmailOptions);
    console.log('normalizedEmailWithOptions: ', normalizedEmailWithOptions);

    return normalizedEmail;
};
