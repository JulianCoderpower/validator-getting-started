module.exports = function isAlpha(validator){
    var name = "John";

    var nameIsAlpha = validator.isAlpha(name);
    console.log('nameIsAlpha: ', nameIsAlpha);

    // The second argument can be used to define the locale
    // default: 'en-US'
    var spanishName = 'Débora';

    var spanishNameIsAlphaWithoutLocale = validator.isAlpha(spanishName);
    console.log('spanishNameIsAlphaWithoutLocale: ', spanishNameIsAlphaWithoutLocale);

    var spanishNameIsAlphaWithLocale = validator.isAlpha(spanishName, 'es-ES');
    console.log('spanishNameIsAlphaWithLocale: ', spanishNameIsAlphaWithLocale);


    return nameIsAlpha;
};