var expect = require('expect.js');
var sinon = require('sinon');
var validator = require('validator');

var isAlpha = require('../sources/isAlpha');
var normalizeEmail = require('../sources/normalizeEmail');

var spy = validator;
spy.isAlpha = sinon.spy(validator, 'isAlpha');
spy.normalizeEmail = sinon.spy(validator, 'normalizeEmail');

describe('Getting start with validator.js', function () {
    it('The method isAlpha() must be invoked', function () {
        var result = isAlpha(spy);
        expect(spy.isAlpha.called).to.be.ok();
        expect(result).to.be.a('boolean');
    });

    it('The method normalizeEmail() must be invoked', function () {
        var result = normalizeEmail(spy);
        expect(spy.normalizeEmail.called).to.be.ok();
        expect(result).to.be.a('string');
        expect(result).to.be('test@te.com');
    });

});